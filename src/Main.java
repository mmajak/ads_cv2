import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args) throws IOException {

        List<String> inputFile = Files.lines(Paths.get("cvicenie2data2019.txt")).collect(Collectors.toList());

        int distance = 0;
        int finalPenalisation = 0;
        int distanceReached = 0;

        while (!inputFile.isEmpty()) {

            int nearestCity = Integer.parseInt(inputFile.get(0));

            if (distance == 0) {
                distance = nearestCity - distanceReached;
                inputFile.remove(0);

                if (!inputFile.isEmpty()) {
                    nearestCity = Integer.parseInt(inputFile.get(0));
                }
            }

            if (getPenalisation(distance) >= getPenalisation(nearestCity - distanceReached)) {
                distance = nearestCity - distanceReached;

                if (!inputFile.isEmpty()) {
                    inputFile.remove(0);
                }

            } else {
                finalPenalisation += getPenalisation(distance);
                distanceReached += distance;

                System.out.println("distance " + distance);
                System.out.println("distance reached " + distanceReached);
                System.out.println("penalisation " + finalPenalisation);
                System.out.println("-------------");

                distance = 0;
            }

        }

        System.out.println(finalPenalisation);

    }


    public static double getPenalisation(final int distance) {
        return Math.pow(400 - distance, 2);
    }

}
